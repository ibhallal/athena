#!/usr/bin/env python

# art-description: Legacy trigger test on data with Physics_HI_v4 menu
# art-type: grid
# art-include: master/Athena
# art-output: *.txt
# art-output: *.log
# art-output: log.*
# art-output: *.out
# art-output: *.err
# art-output: *.log.tar.gz
# art-output: *.new
# art-output: *.json
# art-output: *.root
# art-output: *.pmon.gz
# art-output: *perfmon*
# art-output: *.check*

from TrigValTools.TrigValSteering import Test, ExecStep, CheckSteps
from TrigValTools.TrigValSteering.Input import get_input

ex = ExecStep.ExecStep()
ex.type = 'athena'
ex.job_options = 'TriggerJobOpts/runHLT_standalone_run2.py'
ex.input = ''  # These JO expect BSRDOInput variable instead of normal athena input handling
ex.explicit_input = True
ex.args = '-c \'setMenu="Physics_HI_v4"; BSRDOInput=["{:s}"]\''.format(get_input('data').paths[0])

test = Test.Test()
test.art_type = 'grid'
test.exec_steps = [ex]
test.check_steps = CheckSteps.default_check_steps(test)

import sys
sys.exit(test.run())
